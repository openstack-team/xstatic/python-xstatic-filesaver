Source: python-xstatic-filesaver
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-setuptools,
Standards-Version: 4.2.0
Vcs-Browser: https://salsa.debian.org/openstack-team/xstatic/python-xstatic-filesaver
Vcs-Git: https://salsa.debian.org/openstack-team/xstatic/python-xstatic-filesaver.git
Homepage: https://github.com/openstack/xstatic-filesaver

Package: python3-xstatic-filesaver
Architecture: all
Depends:
 python3-xstatic,
 ${misc:Depends},
 ${python3:Depends},
Description: implements the saveAs() FileSaver interface - XStatic support
 XStatic is a Python web development tool for handling required static data
 files from external projects, such as CSS, images, and JavaScript. It provides
 a lightweight infrastructure to manage them via Python modules that your app
 can depend on in a portable, virtualenv-friendly way instead of using embedded
 copies.
 .
 FileSaver.js implements the saveAs() FileSaver interface in browsers that do
 not natively support it.
 .
 FileSaver.js is the solution to saving files on the client-side, and is
 perfect for webapps that need to generate files, or for saving sensitive
 information that shouldn't be sent to an external server.
